all: init deploy connect

init:
	@echo "Initializing the environment"
	@terraform init
	@terraform plan -var-file=terraform.tfvars

deploy:
	@terraform apply -var-file=terraform.tfvars --auto-approve

ssh-config:
	@if [ ! -e ~/.ssh/config ]; then touch ~/.ssh/config; fi
	@chmod 600 ~/.ssh/config
	@envsubst < scripts/ssh-config >> ~/.ssh/config

connect:
	@ssh bastion

destroy:
	@terraform destroy --auto-approve
 