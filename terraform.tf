# Define required providers
terraform {
    required_version = ">= 0.14.0"
    required_providers {
        openstack = {
            source  = "terraform-provider-openstack/openstack"
            version = "~> 1.35.0"
        }
    }
}

# Configure the OpenStack Provider
provider "openstack" {
#  user_name   = "${OS_USERNAME}"
#  password    = "${OS_PASSWORD}"
#  auth_url    = "${OS_AUTH_URL}"
  region      = "RegionOne"
}